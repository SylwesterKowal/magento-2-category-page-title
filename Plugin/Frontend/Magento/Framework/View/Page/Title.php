<?php


namespace Kowal\CategoryPageTitle\Plugin\Frontend\Magento\Framework\View\Page;

use \Magento\Framework\View\Page\Title as NativeTitle;

class Title
{
    /**
     * @var string
     */
    protected $_pageVarName = 'p';

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->request = $request;
    }

    public function afterGet(
        \Magento\Framework\View\Page\Title $subject,
        $result
    )
    {

        $page = (int)$this->request->getParam($this->_pageVarName, false);
        if ($page) {
            $result .= __(' | Page %1', $page);
        }


        return $result;
    }
}
